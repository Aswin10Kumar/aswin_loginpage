const emailLogin = document.getElementById("email");
const passLogin = document.getElementById("pass");
const errorLogIn = document.getElementById("errLogin");
const form =document.getElementById("form1");

const emailSign = document.getElementById("emailSign");
const passSign = document.getElementById("passSign");
const numberSign = document.getElementById("numberSign");
const nameSign = document.getElementById("nameSign");
const errorSignIn = document.getElementById("errSign");


const emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
const passRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/;

// function logInValidation() 

form.addEventListener("submit", (event) =>{

    event.preventDefault();

    const errorLog = [];
    if (emailRegex.test(emailLogin.value) == false) {
        errorLog.push("email");
    }

    if (passRegex.test(passLogin.value) == false) {
        errorLog.push("password");
    }

    const errorMessage = "Invalid " + errorLog.join(',');

    if (errorLog.length > 0) {
        errorLogIn.style.display = "block";
        errorLogIn.style.color = "rgba(171, 7, 7, 1)";
        errorLogIn.innerHTML = errorMessage;
    }
    else {
        errorLogIn.style.display = "none";
        location.replace("./welcome.html");
        // form.action="./welcome.html"
        // form.setAttribute("action","./index.html")
    }
})

function register() {
    document.getElementsByClassName('signUp')[0].style.display = "block";
    document.getElementsByClassName('login')[0].style.display = "none";
}

function signUpValidation() {

    event.preventDefault();

    const errorLog = [];
    if (emailRegex.test(emailSign.value) == false) {
        errorLog.push("email");
    }

    if (passRegex.test(passSign.value) == false) {
        errorLog.push("password");
    }

    const isNum = +numberSign.value < 1000000000 && +numberSign.value < 9999999999;

    if (isNum) {
        errorLog.push("Mobile number");
    }

    if (nameSign.value.trim().length == 0) {
        errorLog.push("name");
    }
    const errorMessage = "Invalid " + errorLog.join(',');

    if (errorLog.length > 0) {
        errorSignIn.style.display = "block";
        errorSignIn.style.color = "rgba(171, 7, 7, 1)";
        errorSignIn.innerHTML = errorMessage;
    }
    else {
        errorSignIn.style.display = "none";
        signIn();
    }
}

function signIn() {
    document.getElementsByClassName('signUp')[0].style.display = "none";
    document.getElementsByClassName('login')[0].style.display = "block";
}